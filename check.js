import { LightningElement } from 'lwc';
import isguest from '@salesforce/user/isGuest';
export default class CheckGuestUserInLWC extends LightningElement {
    
    // Expose the value of @salesforce/user/isGuest in the template.
    isGuestUser = isguest;
}